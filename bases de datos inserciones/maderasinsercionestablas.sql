-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: maderasins
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `idcargo` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombrecargo` varchar(50) NOT NULL,
  PRIMARY KEY (`idcargo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'asesor_comercial'),(2,'gerente'),(3,'jefe_produccion'),(4,'jefe_bodega');
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idcliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidtipodocumento` bigint(20) DEFAULT NULL,
  `numerodocumentocliente` bigint(20) DEFAULT NULL,
  `primernombre` varchar(50) DEFAULT NULL,
  `primerapellido` varchar(50) DEFAULT NULL,
  `fkidgenero` bigint(20) DEFAULT NULL,
  `celular` bigint(20) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fkidempleado` bigint(20) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  `fkideps` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idcliente`),
  KEY `fkideps` (`fkideps`),
  KEY `fkidempleado` (`fkidempleado`),
  KEY `fktipodocumento` (`fkidtipodocumento`),
  KEY `fkidestado` (`fkidestado`),
  KEY `fkidgenero` (`fkidgenero`),
  CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`fkidtipodocumento`) REFERENCES `tipodocumento` (`idtipodocumento`),
  CONSTRAINT `cliente_ibfk_2` FOREIGN KEY (`fkideps`) REFERENCES `eps` (`ideps`),
  CONSTRAINT `cliente_ibfk_3` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`),
  CONSTRAINT `cliente_ibfk_4` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `cliente_ibfk_5` FOREIGN KEY (`fkidgenero`) REFERENCES `genero` (`idgenero`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,2,111111111,'michael','escudero',1,21212121,'cll 111',1,1,1),(2,2,111111111,'michael','escudero',1,21212121,'cll 111',1,1,1),(3,2,222222222,'jose','escudero',1,1111111,'cll 2222',1,1,1),(4,2,33333333,'brandon','escudero',1,333333333333,'cll 333',1,1,1),(5,2,4444444,'juana','escudero',1,4444444444,'cll 444',1,1,1),(6,2,5555555,'stella','avila',1,555555555,'cll 555',1,1,1),(7,2,6666666,'jose','casas',1,6666666,'cll 666',1,1,1),(8,2,777777,'kevin','duarte',1,7777777777,'cll 777',5,1,1),(9,2,88888888888,'carol','duarte',1,88888888,'cll888',6,1,1),(10,2,999999999,'camilo','duarte',1,99999,'cll999',9,1,1),(11,2,10000000000,'andres','benavides',1,1000,'cll100',9,1,3),(12,2,10000000001,'maria','avila',1,10001,'cll101',9,1,4),(13,2,100122222,'diego','boada',1,10122,'cll201',6,1,4),(14,2,10033333,'carlos','avila',1,10133,'cll203',5,1,3),(15,2,100444,'mauricio','avila',1,10144,'cll204',7,1,2),(16,2,54645654,'alex','castillo',1,21212122,'cll 14',21,1,4),(17,2,54645654,'alex','castillo',1,21212122,'cll 14',21,1,4),(18,1,7545546,'sayonara','plata',1,54545454,'cll 14',18,1,4),(19,1,7545546,'sayonara','plata',1,54545454,'cll 14',18,1,4),(20,1,5454556,'juliana','lopez',2,4545454,'cll 51',30,1,1),(21,1,4545645,'vanesa','sanches',2,64564545,'cll 125',11,1,3),(22,1,5454556,'juliana','lopez',2,4545454,'cll 51',30,1,1),(23,1,4545645,'vanesa','sanches',2,64564545,'cll 125',11,1,3),(24,2,5454545,'alejandra','rojas',2,65656565,'cll 25',29,1,4),(25,2,8989898965565,'lola','loleta',2,45454,'cll 20',15,1,4),(26,2,5454545,'alejandra','rojas',2,65656565,'cll 25',29,1,4),(27,2,8989898965565,'lola','loleta',2,45454,'cll 20',15,1,4),(28,2,560230326,'lol','lososo',1,33363325,'cll 125',27,1,4),(29,2,560230326,'lol','lososo',1,33363325,'cll 125',27,1,4),(30,1,3556566,'camila','bello',2,55555,'cll 25',2,1,2),(31,1,3556566,'camila','bello',2,55555,'cll 25',2,1,2);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `idcontrato` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidcliente` bigint(20) NOT NULL,
  `fkidempleado` bigint(20) NOT NULL,
  `fecharegistro` timestamp NULL DEFAULT NULL,
  `fehcaentrega` date DEFAULT NULL,
  `descripcion` text,
  `fechavencimiento` date DEFAULT NULL,
  PRIMARY KEY (`idcontrato`),
  KEY `fkidempleado` (`fkidempleado`),
  KEY `fkidcliente` (`fkidcliente`),
  CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`),
  CONSTRAINT `contrato_ibfk_2` FOREIGN KEY (`fkidcliente`) REFERENCES `cliente` (`idcliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ctrlcalidadpf`
--

DROP TABLE IF EXISTS `ctrlcalidadpf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctrlcalidadpf` (
  `idctrlcalidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `aprobacion` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`idctrlcalidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ctrlcalidadpf`
--

LOCK TABLES `ctrlcalidadpf` WRITE;
/*!40000 ALTER TABLE `ctrlcalidadpf` DISABLE KEYS */;
/*!40000 ALTER TABLE `ctrlcalidadpf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefacturacompra`
--

DROP TABLE IF EXISTS `detallefacturacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallefacturacompra` (
  `iddetallecompra` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidfacturacompra` bigint(20) DEFAULT NULL,
  `fkidmaterial` bigint(20) DEFAULT NULL,
  `cantidad` bigint(20) DEFAULT NULL,
  `totalpago` double DEFAULT NULL,
  PRIMARY KEY (`iddetallecompra`),
  KEY `fkidfacturacompra` (`fkidfacturacompra`),
  KEY `fkidmaterial` (`fkidmaterial`),
  CONSTRAINT `detallefacturacompra_ibfk_1` FOREIGN KEY (`fkidfacturacompra`) REFERENCES `facturacompra` (`idfacturacompra`),
  CONSTRAINT `detallefacturacompra_ibfk_2` FOREIGN KEY (`fkidmaterial`) REFERENCES `inventariomatpri` (`idmaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefacturacompra`
--

LOCK TABLES `detallefacturacompra` WRITE;
/*!40000 ALTER TABLE `detallefacturacompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallefacturacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefacturaventa`
--

DROP TABLE IF EXISTS `detallefacturaventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallefacturaventa` (
  `iddetalleventa` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidfacturaventa` bigint(20) NOT NULL,
  `fkidproductofinal` bigint(20) DEFAULT NULL,
  `cantidad` bigint(20) DEFAULT NULL,
  `totalpago` double DEFAULT NULL,
  PRIMARY KEY (`iddetalleventa`),
  KEY `fkidfacturaventa` (`fkidfacturaventa`),
  KEY `idproductofinal` (`fkidproductofinal`),
  CONSTRAINT `detallefacturaventa_ibfk_1` FOREIGN KEY (`fkidfacturaventa`) REFERENCES `facturaventa` (`idfacturaventa`),
  CONSTRAINT `detallefacturaventa_ibfk_2` FOREIGN KEY (`fkidproductofinal`) REFERENCES `invprofinal` (`idproducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefacturaventa`
--

LOCK TABLES `detallefacturaventa` WRITE;
/*!40000 ALTER TABLE `detallefacturaventa` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallefacturaventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `idempleado` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidtipodocumento` bigint(20) DEFAULT NULL,
  `numerodocumentoempleado` bigint(20) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `primernombre` varchar(50) NOT NULL,
  `segundonombre` varchar(50) DEFAULT NULL,
  `primerapellido` varchar(50) DEFAULT NULL,
  `segundoapellido` varchar(50) DEFAULT NULL,
  `fkidcargo` bigint(20) DEFAULT NULL,
  `fkidgenero` bigint(20) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `celular` bigint(20) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `usuario` varchar(100) DEFAULT NULL,
  `contraseña` varchar(100) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  `fkideps` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idempleado`),
  KEY `fktipodocumento` (`fkidtipodocumento`),
  KEY `fkidcargo` (`fkidcargo`),
  KEY `fkideps` (`fkideps`),
  KEY `fkidestado` (`fkidestado`),
  KEY `fkidgenero` (`fkidgenero`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`fkidtipodocumento`) REFERENCES `tipodocumento` (`idtipodocumento`),
  CONSTRAINT `empleado_ibfk_2` FOREIGN KEY (`fkidcargo`) REFERENCES `cargo` (`idcargo`),
  CONSTRAINT `empleado_ibfk_3` FOREIGN KEY (`fkideps`) REFERENCES `eps` (`ideps`),
  CONSTRAINT `empleado_ibfk_4` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `empleado_ibfk_5` FOREIGN KEY (`fkidgenero`) REFERENCES `genero` (`idgenero`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,2,99011502829,'2000-02-02','michael','armando','escudero','avila',1,1,6893045,325252525,'cll125','pacol@4','maicol','123',1,1),(2,2,99011502829,'2000-08-02','andres','benavides','beltran','castellanos',1,1,6896585,3253653698,'cll12','ab@45','andy','25',1,2),(3,1,65656565,'2010-08-02','stella','rosa','beltran','avila',1,2,6896695,69696954,'cll18','rsav@5','rouss','22',1,1),(4,2,995698745,'2008-02-06','jose','david','escudero','avila',2,1,989898,3253646487,'cll 145','je@54.com','je','6565',1,2),(5,1,36523658,'2000-02-25','brandon','javier','escudero','avila',1,1,6523698,3254548745,'cll 14','be@64.com','be','6565',1,3),(6,2,64565456,'2100-02-25','juana','valentina','escudero','avila',1,2,3265356,213244123,'cll 556','je@65.com','je','3154',1,4),(7,2,52395124,'2010-02-05','rosa','stella','avila','beltran',3,2,65454156,123654897,'cll 5','RA@65.com','ra','1212',1,2),(8,2,98543232,'1989-02-05','jose','armando','escudero','casas',1,1,3232653,3232323236,'cll 445','JA@64.com','ja','1212',1,2),(9,2,88526956,'1978-02-05','kevin','alfonso','duarte','avila',1,1,3232653,3232323236,'cll 45','KD@64.com','KD','1212',1,2),(10,2,67467465,'1998-02-05','maria','fernanda','duarte','avila',1,2,3464646,565666100,'cll 85','mf@64.com','mf','1212',1,4),(11,2,5656565,'1998-08-06','carol','natalia','duarte','avila',1,2,1472583,6545125487,'cll 123','cn@64.com','cn','5265',1,2),(12,2,98998989,'0000-00-00','aura','maria','avila','beltran',1,2,1656653,36962586,'cll 3','kj@64.com','aa','5265',1,2),(13,2,98989895,'0000-00-00','alfonso','','duarte','camargo',3,2,8787877,65656264,'cll 3','54@64.com','alfonso','5265',1,2),(14,2,3265986532,'1988-01-03','mauricio','javier','beltran','avila',2,1,854746565,1472555552,'cll 84','84@64.com','hola','5265',1,2),(15,2,65986598,'1988-05-03','samuel','david','beltran','avila',2,1,8547465,1472525552,'cll 125','5@64.com','lol','5265',1,1),(16,2,8787875,'1988-05-03','harold','andres','beltran','avila',2,1,21212121,454545445,'cll 145','544@64.com','lola','87',1,3),(17,2,7787875,'1988-08-03','carlos','','beltran','avila',2,1,28212121,1111111111,'cll 111','111@64.com','111','87',1,3),(18,2,778700875,'1988-07-03','camilo','andres','beltran','avila',2,1,28812121,22222222222,'cll 222','222@64.com','222','87',1,2),(19,2,65656565,'1988-08-03','andres','antonio','benavides','avila',2,1,66656656,33333333333,'cll 333','333@64.com','333','87',1,2),(20,2,6464646495,'1988-09-03','omaida','','escudero','casas',2,1,666656656,4444444444,'cll 444','444@64.com','444','87',1,2),(21,2,656656565,'1988-10-03','ana','graciela','beltran','csallas',2,1,45679874,55555555555,'cll 555','555@64.com','555','87',1,2),(22,2,111111111,'1988-11-03','jesus','david','avila','csallas',2,1,45569874,6666666666,'cll 666','666@64.com','666','87',1,2),(23,2,2222222,'1985-12-03','rosalba','','casa','rincon',2,1,4556574,77777777,'cll 777','777.com','777','87',1,2),(24,2,33333333,'1985-12-01','paula','andrea','casas','rincon',2,1,4556024,888888888,'cll 888','888.com','888','87',1,2),(25,2,4444444,'1985-12-02','viviana','','casas','rincon',2,1,4556874,999999999,'cll 999','999.com','999','87',1,2),(26,2,5555555,'1995-12-02','sergio','andres','guzman','rivera',2,1,45563024,1010101010,'cll 101','101.com','101','87',1,2),(27,2,66666666,'1995-12-02','angelo','steven','carvajal','gallardo',2,1,45566024,1212121212,'cll 112','112.com','112','87',1,2),(28,2,77777777,'1995-01-02','juan','pablo','rodriges','ballona',2,1,45566024,1212121212,'cll 112','112.com','112','87',1,2),(29,2,888888888,'1994-01-02','sergio','david','hernadez','goyeneche',2,1,45566024,1212121212,'cll 112','112.com','112','87',1,2),(30,2,9999999999,'1994-01-02','luis','alejandro','gonzalez','montañes',2,1,45566024,1212121212,'cll 112','112.com','112','87',1,2);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `idempresa` bigint(20) NOT NULL AUTO_INCREMENT,
  `nit` bigint(20) DEFAULT NULL,
  `nombreempresa` varchar(100) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `fkidtipoempresa` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idempresa`),
  KEY `fkidtipoempresa` (`fkidtipoempresa`),
  CONSTRAINT `empresa_ibfk_1` FOREIGN KEY (`fkidtipoempresa`) REFERENCES `tipoempresa` (`idtipoempresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eps`
--

DROP TABLE IF EXISTS `eps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eps` (
  `ideps` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreeps` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ideps`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eps`
--

LOCK TABLES `eps` WRITE;
/*!40000 ALTER TABLE `eps` DISABLE KEYS */;
INSERT INTO `eps` VALUES (1,'famisanar'),(2,'salud_total'),(3,'colsanitas'),(4,'cafesalud'),(5,'policia');
/*!40000 ALTER TABLE `eps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `idestado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreestado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idestado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'activo'),(2,'inactivo');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacompra`
--

DROP TABLE IF EXISTS `facturacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturacompra` (
  `idfacturacompra` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidempleado` bigint(20) DEFAULT NULL,
  `fkidproveedor` bigint(20) DEFAULT NULL,
  `fecharegistro` timestamp NULL DEFAULT NULL,
  `totalpago` double DEFAULT NULL,
  `observacion` text,
  PRIMARY KEY (`idfacturacompra`),
  KEY `fkidproveedor` (`fkidproveedor`),
  KEY `fkidempleado` (`fkidempleado`),
  CONSTRAINT `facturacompra_ibfk_1` FOREIGN KEY (`fkidproveedor`) REFERENCES `proveedor` (`idproveedor`),
  CONSTRAINT `facturacompra_ibfk_2` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacompra`
--

LOCK TABLES `facturacompra` WRITE;
/*!40000 ALTER TABLE `facturacompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaventa`
--

DROP TABLE IF EXISTS `facturaventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturaventa` (
  `idfacturaventa` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidempleado` bigint(20) DEFAULT NULL,
  `fkidcliente` bigint(20) DEFAULT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `totalpago` double DEFAULT NULL,
  `observacion` text,
  PRIMARY KEY (`idfacturaventa`),
  KEY `fkidcliente` (`fkidcliente`),
  KEY `fkidempleado` (`fkidempleado`),
  CONSTRAINT `facturaventa_ibfk_1` FOREIGN KEY (`fkidcliente`) REFERENCES `cliente` (`idcliente`),
  CONSTRAINT `facturaventa_ibfk_2` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaventa`
--

LOCK TABLES `facturaventa` WRITE;
/*!40000 ALTER TABLE `facturaventa` DISABLE KEYS */;
/*!40000 ALTER TABLE `facturaventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genero`
--

DROP TABLE IF EXISTS `genero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genero` (
  `idgenero` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombregenero` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idgenero`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genero`
--

LOCK TABLES `genero` WRITE;
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` VALUES (1,'masculino'),(2,'femenino');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventariomatpri`
--

DROP TABLE IF EXISTS `inventariomatpri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventariomatpri` (
  `idmaterial` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidtipomaterial` bigint(20) DEFAULT NULL,
  `cantidad` bigint(20) DEFAULT NULL,
  `fkidempleado` bigint(20) DEFAULT NULL,
  `fkidproveedor` bigint(20) DEFAULT NULL,
  `observacion` text,
  `preciounitario` double DEFAULT NULL,
  PRIMARY KEY (`idmaterial`),
  KEY `fkidproveedor` (`fkidproveedor`),
  KEY `fkidtipomaterial` (`fkidtipomaterial`),
  KEY `fkidempleado` (`fkidempleado`),
  CONSTRAINT `inventariomatpri_ibfk_1` FOREIGN KEY (`fkidproveedor`) REFERENCES `proveedor` (`idproveedor`),
  CONSTRAINT `inventariomatpri_ibfk_2` FOREIGN KEY (`fkidtipomaterial`) REFERENCES `tipomateriaprima` (`idmateriaprima`),
  CONSTRAINT `inventariomatpri_ibfk_3` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventariomatpri`
--

LOCK TABLES `inventariomatpri` WRITE;
/*!40000 ALTER TABLE `inventariomatpri` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventariomatpri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invprofinal`
--

DROP TABLE IF EXISTS `invprofinal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invprofinal` (
  `idproducto` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidtipoproducto` bigint(20) DEFAULT NULL,
  `fkidctrlcalidad` bigint(20) DEFAULT NULL,
  `cantidad` bigint(20) DEFAULT NULL,
  `fkidempleado` bigint(20) DEFAULT NULL,
  `preciounitario` double DEFAULT NULL,
  `observacion` text,
  PRIMARY KEY (`idproducto`),
  KEY `fkidempleado` (`fkidempleado`),
  KEY `fkidtipoproducto` (`fkidtipoproducto`),
  KEY `fkidctrlcalidad` (`fkidctrlcalidad`),
  CONSTRAINT `invprofinal_ibfk_1` FOREIGN KEY (`fkidctrlcalidad`) REFERENCES `ctrlcalidadpf` (`idctrlcalidad`),
  CONSTRAINT `invprofinal_ibfk_2` FOREIGN KEY (`fkidtipoproducto`) REFERENCES `tipoproducto` (`idtipoproducto`),
  CONSTRAINT `invprofinal_ibfk_3` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invprofinal`
--

LOCK TABLES `invprofinal` WRITE;
/*!40000 ALTER TABLE `invprofinal` DISABLE KEYS */;
/*!40000 ALTER TABLE `invprofinal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `idproveedor` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidtipodocumento` bigint(20) DEFAULT NULL,
  `numdocproveedor` bigint(20) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `nombresproveedor` varchar(100) DEFAULT NULL,
  `apellidosproveedor` varchar(100) DEFAULT NULL,
  `fkidgenero` bigint(20) DEFAULT NULL,
  `celular` bigint(20) DEFAULT NULL,
  `telefono` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fkidempleado` bigint(20) DEFAULT NULL,
  `fkidestado` bigint(20) DEFAULT NULL,
  `fecharegistro` timestamp NULL DEFAULT NULL,
  `fkidempresaafiliado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idproveedor`),
  KEY `fkidtipodocumento` (`fkidtipodocumento`),
  KEY `fkidempleado` (`fkidempleado`),
  KEY `fkidgenero` (`fkidgenero`),
  KEY `fkidestado` (`fkidestado`),
  KEY `fkidempresaafiliado` (`fkidempresaafiliado`),
  CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`fkidtipodocumento`) REFERENCES `tipodocumento` (`idtipodocumento`),
  CONSTRAINT `proveedor_ibfk_2` FOREIGN KEY (`fkidempleado`) REFERENCES `empleado` (`idempleado`),
  CONSTRAINT `proveedor_ibfk_3` FOREIGN KEY (`fkidgenero`) REFERENCES `genero` (`idgenero`),
  CONSTRAINT `proveedor_ibfk_4` FOREIGN KEY (`fkidestado`) REFERENCES `estado` (`idestado`),
  CONSTRAINT `proveedor_ibfk_5` FOREIGN KEY (`fkidempresaafiliado`) REFERENCES `empresa` (`idempresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumento` (
  `idtipodocumento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombretipodocumento` varchar(50) NOT NULL,
  PRIMARY KEY (`idtipodocumento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumento`
--

LOCK TABLES `tipodocumento` WRITE;
/*!40000 ALTER TABLE `tipodocumento` DISABLE KEYS */;
INSERT INTO `tipodocumento` VALUES (1,'tarjeta_identidad'),(2,'cedula');
/*!40000 ALTER TABLE `tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoempresa`
--

DROP TABLE IF EXISTS `tipoempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoempresa` (
  `idtipoempresa` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombretipoempresa` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idtipoempresa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoempresa`
--

LOCK TABLES `tipoempresa` WRITE;
/*!40000 ALTER TABLE `tipoempresa` DISABLE KEYS */;
INSERT INTO `tipoempresa` VALUES (1,'publica'),(2,'privada');
/*!40000 ALTER TABLE `tipoempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipomateriaprima`
--

DROP TABLE IF EXISTS `tipomateriaprima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipomateriaprima` (
  `idmateriaprima` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombretipomaterial` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idmateriaprima`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipomateriaprima`
--

LOCK TABLES `tipomateriaprima` WRITE;
/*!40000 ALTER TABLE `tipomateriaprima` DISABLE KEYS */;
INSERT INTO `tipomateriaprima` VALUES (1,'bases'),(2,'tablas'),(3,'madera'),(4,'caucho'),(5,'algodon');
/*!40000 ALTER TABLE `tipomateriaprima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoproducto`
--

DROP TABLE IF EXISTS `tipoproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoproducto` (
  `idtipoproducto` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombretipoproducto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idtipoproducto`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoproducto`
--

LOCK TABLES `tipoproducto` WRITE;
/*!40000 ALTER TABLE `tipoproducto` DISABLE KEYS */;
INSERT INTO `tipoproducto` VALUES (1,'cama'),(2,'sofa'),(3,'comedor'),(4,'bife'),(5,'cillas'),(6,'almohadas');
/*!40000 ALTER TABLE `tipoproducto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-26 11:22:13
