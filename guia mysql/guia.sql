-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: bdguia
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `idalumno` bigint(20) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(20) DEFAULT NULL,
  `nombres` varchar(30) DEFAULT NULL,
  `apellidos` varchar(30) DEFAULT NULL,
  `correo` varchar(30) DEFAULT NULL,
  `TelefonoCelular` varchar(20) DEFAULT NULL,
  `genero` tinyint(1) DEFAULT NULL,
  `perfil` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idalumno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curso`
--

DROP TABLE IF EXISTS `curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curso` (
  `codigocurso` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombrecurso` varchar(30) DEFAULT NULL,
  `descripcion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`codigocurso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curso`
--

LOCK TABLES `curso` WRITE;
/*!40000 ALTER TABLE `curso` DISABLE KEYS */;
/*!40000 ALTER TABLE `curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especialidad`
--

DROP TABLE IF EXISTS `especialidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `especialidad` (
  `idespecialidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombreEspecialidad` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idespecialidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especialidad`
--

LOCK TABLES `especialidad` WRITE;
/*!40000 ALTER TABLE `especialidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `especialidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesor`
--

DROP TABLE IF EXISTS `profesor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profesor` (
  `identificadorprofesor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(30) DEFAULT NULL,
  `apellido` varchar(30) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `correo` varchar(30) DEFAULT NULL,
  `contraseña` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`identificadorprofesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesor`
--

LOCK TABLES `profesor` WRITE;
/*!40000 ALTER TABLE `profesor` DISABLE KEYS */;
/*!40000 ALTER TABLE `profesor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesor_especialidad`
--

DROP TABLE IF EXISTS `profesor_especialidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profesor_especialidad` (
  `fkidentificadorprofesor` bigint(20) DEFAULT NULL,
  `fkidespecialidad` bigint(20) DEFAULT NULL,
  KEY `profesor_especialidadprofesor` (`fkidentificadorprofesor`),
  KEY `profesor_especialidadespecialidad` (`fkidespecialidad`),
  CONSTRAINT `profesor_especialidad_ibfk_1` FOREIGN KEY (`fkidentificadorprofesor`) REFERENCES `profesor` (`identificadorprofesor`),
  CONSTRAINT `profesor_especialidad_ibfk_2` FOREIGN KEY (`fkidespecialidad`) REFERENCES `especialidad` (`idespecialidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesor_especialidad`
--

LOCK TABLES `profesor_especialidad` WRITE;
/*!40000 ALTER TABLE `profesor_especialidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `profesor_especialidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacion`
--

DROP TABLE IF EXISTS `programacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programacion` (
  `idprogramacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `fechainicio` datetime DEFAULT NULL,
  `fechafin` datetime DEFAULT NULL,
  `cupo` int(11) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `costo` int(11) DEFAULT NULL,
  `fkcodigocurso` bigint(20) DEFAULT NULL,
  `fkidsalon` bigint(20) DEFAULT NULL,
  `fkidentificadorprofesor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idprogramacion`),
  KEY `programacioncurso` (`fkcodigocurso`),
  KEY `programacionsalon` (`fkidsalon`),
  KEY `programacionprofesor` (`fkidentificadorprofesor`),
  CONSTRAINT `programacion_ibfk_1` FOREIGN KEY (`fkcodigocurso`) REFERENCES `curso` (`codigocurso`),
  CONSTRAINT `programacion_ibfk_2` FOREIGN KEY (`fkidsalon`) REFERENCES `salon` (`idsalon`),
  CONSTRAINT `programacion_ibfk_3` FOREIGN KEY (`fkidentificadorprofesor`) REFERENCES `profesor` (`identificadorprofesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programacion`
--

LOCK TABLES `programacion` WRITE;
/*!40000 ALTER TABLE `programacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `programacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacion_alumno`
--

DROP TABLE IF EXISTS `programacion_alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programacion_alumno` (
  `idprogramacionAlumno` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkidalumno` bigint(20) DEFAULT NULL,
  `fkidprogramacion` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idprogramacionAlumno`),
  KEY `programacion_alumnoalumno` (`fkidalumno`),
  KEY `programacion_alumnoprogramacion` (`fkidprogramacion`),
  CONSTRAINT `programacion_alumno_ibfk_1` FOREIGN KEY (`fkidalumno`) REFERENCES `alumno` (`idalumno`),
  CONSTRAINT `programacion_alumno_ibfk_2` FOREIGN KEY (`fkidprogramacion`) REFERENCES `programacion` (`idprogramacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programacion_alumno`
--

LOCK TABLES `programacion_alumno` WRITE;
/*!40000 ALTER TABLE `programacion_alumno` DISABLE KEYS */;
/*!40000 ALTER TABLE `programacion_alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salon`
--

DROP TABLE IF EXISTS `salon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salon` (
  `idsalon` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombresalon` varchar(30) DEFAULT NULL,
  `ubicacion` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idsalon`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salon`
--

LOCK TABLES `salon` WRITE;
/*!40000 ALTER TABLE `salon` DISABLE KEYS */;
/*!40000 ALTER TABLE `salon` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-25 11:44:19
